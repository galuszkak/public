# OpenCraft Tutorials

This directory exists to host a set of tutorials on common tasks OpenCraft members need to perform. They're a bit much to put in the main handbook but siloing them in our private docs is unnecessary.

It is our hope that most of the information in these tutorials will eventually be upstreamed in one fashion or another into upstream documentation or courses. If this directory has nothing but the README in it, we're in good shape.

