# How to deploy LPD

Purpose: In this tutorial we'll be deploying LPD to AWS using [ansible-playbook](https://github.com/open-craft/ansible-playbooks) and [ansible-secrets](https://github.com/open-craft/ansible-secrets). The tutorial is based on [another AWS deployment tutorial](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_ELB_deployment_tutorial.md).

<br><br><br>

--------------

**TODO**: These instructions need to be replaced with Terraform code templates. All new AWS infrastructure should be defined and managed by Terraform.

Do not manually provision any new LPDs using the steps below! Instead please do it using Terraform and then replace these instructions with the new Terraform-based instructions. See [the Terraformed Infrastructure epic](https://tasks.opencraft.com/browse/SE-2436) for details.

--------------

<br><br><br>

## Create config files for your domain

Create `name-of-your-domain.yml` file in the `host_vars` directory of [ansible-secrets](https://github.com/open-craft/ansible-secrets). (You may want to have a look at other files in this directory to see how they're structured.) You will keep most of the configuration secrets for your LPD instance in the file you have just created.

## Create EC2 instance

We will be using a medium [EC2](https://aws.amazon.com/ec2) instance called `lpd` to host the LPD application together with few background jobs (log backups, sanity checker etc.)

Log into the AWS console and go to Services > EC2 > Instances. Choose your preferred region using the drop-down in the top right corner. Click the *Launch Instance* button, and follow the steps to configure the instance. For most configuration steps you can keep default values, unless specified below.

### Step 1. Choose AMI

We will be using the *Ubuntu 16.04 LTS (Xenial Xerus)* operating system. We will need to locate the ID of the most recent official *Ubuntu 16.04* AWS image.

Go to https://cloud-images.ubuntu.com/locator/ and search for version `16.04` `amd64` `ebs-ssd` instance in your preferred AWS region, for example `us-east-1`. If `ebs-ssd` is not available in your region, pick `hvm-ssd` instead. If there are multiple AMIs available, select the most recently released one. Copy the AMI ID of the image you selected (it will look something like `ami-d8132bb0`).

Choose the *Community AMIs* tab and paste the AMI ID into the search box. The AWS image corresponding to that ID should show up. Click the *Select* button.

### Step 2. Choose Instance Type

Select the General Purpose `t2.medium` instance type.

### Step 3. Configure Instance

Ensure the Network setting is set to the default VPC, *not* EC2-Classic.

### Step 4. Add Storage

Increase the root volume size to 10 GB.
Add another EBS volume for logs, set its size to 8GB. Write down its `device` name.

### Step 5. Tag Instance

Give it a `Name` type tag with a value that will let you easily identify the instance in the EC2 dashboard (e.g. `lpd-production`).

### Step 6. Configure Security Group

Create a new security group, and name it with the instance name, i.e. `lpd-production`.

The `lpd-production` group should have three rules defined:

* `SSH`, port `22`, source `Anywhere`
* `HTTP`, port `80`, source `Anywhere` (used to access LPD)
* `HTTPS`, port `443`, source `Anywhere` (used to access the LPD over https)

Click the *Launch* button. A popup will come up that will ask you to select or create a key pair that you will use to access your instance. If you don't already have an existing key pair that you want to use, select *Create a new key pair* and give it a recognizable name. Click *Download Key Pair* and store the downloaded `.pem` file in a safe location (make sure you do it - if you loose your key, you won't be able to SSH into EC2 instance and you will need to deploy to a new EC2 instance).

Click the *Instances* button to see the new instance. Ensure that it's added to the `lpd-production` security group.

After the instance is fully initialized, SSH into it using the key file you used when creating this instance:

```
ssh -i path/to/keyfile.pem ubuntu@ec2-xx-xx-xx-xx.compute-1.amazonaws.com
```

Install all available *Ubuntu 16.04* updates with:

```
sudo apt-get update && sudo apt-get -y upgrade
```

Check what's the Ubuntu name of the EBS volume for logs you created in "Add Storage" step above. In order to do so, type:
```
lsblk
```

This should return something like this:
```
NAME    MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
xvda    202:0    0  10G  0 disk
└─xvda1 202:1    0  10G  0 part /
xvdf    202:80   0   8G  0 disk
```

Check which device has a size of 8GB (or whatever value you set it to when completing the "Add Storage" step above) and write down its name. Set `LPD_LOG_DOWNLOAD_VOLUME_DEVICE_ID` to the name of the device in the configuration file you created at the beginning of this tutorial (`name-of-your-domain.yml`).

### Elastic IP

We will create an Elastic IP and assign it to our EC2 instance. An Elastic IP makes it possible to perform updates or replace servers without downtime. When a new server is ready, you can simply switch the Elastic IP to point to the new server.

Click the *Elastic IPs* link In the EC2 dashboard (located under *Network & Security*). Click the *Allocate New Address* button, select EC2, and click *Yes, Allocate*.

Select the new Elastic IP from the list, and click the *Actions ->Associate Address*. Select your EC2 instance from the dropdown and click *Associate*.

Your EC2 instance can now be reached through the Elastic IP.

You may want to associate the Elastic IP with a proper domain through Gandi and add the domain to `hosts` file in ansible-secrets.

## Create database using RDS (optional)

If you don't have a database ready for your LPD deployment, you may want to use [Amazon RDS](https://aws.amazon.com/rds) to create a MySQL db instance. If that's the case, then:

In the AWS console, navigate to Services > RDS > Databases and click the *Create database* button to create a new instance. Choose MySQL from the Engine Selection list. In the next step do NOT choose Multi-AZ and Provisioned IOPS Storage, unless you know you need those.

On the following pages, make the settings given below. Everything that's not listed can be left at the default value.

Setting name                        | value
-------------------------------------------------|-------------------------------
DB engine version                   | 5.6.x (pick the latest version)
DB Instance Class                   | db.t2.medium
Multi-AZ Deployment             | No
Storage Type                        | General Purpose (SSD)
Allocated Storage                   | 20 GB
DB Instance Identifier          | arbitrary, e.g. "lpd_production"
Master Username/Password       | arbitrary, write them down
Publicly Accessible                 | No
VPC Security Group(s)                  | default (VPC)
Database Name                       | arbitrary, write it down
Backup Retention Period              | 14 days
Auto Minor Version Upgrade        | No

Once the RDS instance is set up, it should be accessible from the EC2 instance. Test this by shelling into EC2 instance and typing the following, using the RDS endpoint host name:

```
telnet xxxxxxxxx.rds.amazonaws.com 3306
```

Confirm that you see a "Connected to ..." message. Type Ctrl-D to exit the telnet shell.

#### Create LPD db user and grant access to LPD db

Shell into EC2 instance and install mysql-client:
```
sudo apt-get install mysql-client
```

Using the client, shell into RDS instance:
```
mysql -h ******.rds.amazonaws.com -P 3306 -u <db_master_username> -p
```

This will ask for password, pass the master one you set when launching RDS instance.

Come up with LPD db username and password, write them down and use them to execute the following commands (replace `<lpd_db_name>` with the db name you set when launching RDS instance).
```
mysql> CREATE USER '<lpd_db_username>'@'%' IDENTIFIED BY '<lpd_db_password>';
mysql> GRANT ALL PRIVILEGES ON <lpd_db_name>.* TO '<lpd_db_username>'@'%';
```

## Obtain database credentials

Save database credentials from previous step (`<lpd_db_username>` and `<lpd_db_password>`) to the configuration file you created at the beginning of this tutorial (`name-of-your-domain.yml`), and specify database host and database name:

- Set `LPD_DB_HOST` to `******.rds.amazonaws.com`.
- Set `LPD_DB_NAME` to `<lpd_db_name>`.
- Set `LPD_DB_USERNAME` to `<lpd_db_username>`.
- Set `LPD_DB_PASSWORD` to `<lpd_db_password>`.

## Create S3 bucket for PDF exports

Each LPD deployment should have its own S3 bucket for storing copies of PDF exports of learner profile data. To create and secure a bucket, and to connect it to the LPD deployment you are creating, follow these steps:

### Create IAM user

1. In the AWS console, navigate to Services > IAM > Users and click *Add user*.

1. Enter a username (e.g. `lpd-prod`) and select *Programmatic access* for the *Access type* setting.

1. When you get to the *Permissions* step, click *Create group*, enter a name for the new group (e.g. `s3-lpd-prod`), and click *Create group*. (You can skip adding a policy to the group at this step.) Make sure that the new group is selected for the user you are creating, then proceed to the next step.

1. When you get to the *Tags* step, consider adding a tag such as `tool` with a value of `lpd` (to provide some additional context on the identity and purpose of the user you are creating).

1. Use the *Review* step to double-check settings, then click *Create user*.

1. Make a note of relevant settings on the next page (i.e., *Access key ID* and *Secret access key*), then click *Close*.

### Create bucket

1. In the AWS console, navigate to Services > S3 and click *Create bucket*.

1. Enter a bucket name (e.g. `lpd-prod`) and proceed to the next step.

1. On the *Properties* screen:

    1. Under *Default encryption*, select *Automatically encrypt objects when they are stored in S3*. (This will give you two options, AES-256 and AWS-KMS. The default of AES-256 is OK to keep.)
    1. Consider adding a tag such as `tool` with a value of `lpd` (to provide some additional context on the purpose of the bucket you are creating), then proceed to the next step.

1. On the *Block public access* screen, uncheck the *Block all public access* option to turn it off, then check the following options to turn them on:

    - *Block public access to buckets and objects granted through new public bucket policies*
    - *Block public and cross-account access to buckets and objects through any public bucket policies*

    The following options should remain unchecked (i.e., off):

    - *Block public access to buckets and objects granted through new access control lists (ACLs)*
    - *Block public access to buckets and objects granted through any access control lists (ACLs)*

1. Use the *Review* step to double-check settings, then click *Create bucket*.

### Create bucket policy

1. In the AWS console, navigate to Services > IAM > Groups and select the group you created earlier.

1. Under *Managed Policies*, make sure it says *There are no managed policies attached to this group.*

1. Under *Inline Policies*, click *Create Group Policy*, then select *Custom Policy* and click *Select*.

1. Provide a name for the policy (e.g. `AllowUpload`) in the editor that comes up, and paste the following content into the *Policy Document* field:

    ```
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "AllowUpload",
          "Effect": "Allow",
          "Action": [
            "s3:PutObject",
            "s3:PutObjectAcl"
          ],
          "Resource": [
            "arn:aws:s3:::<bucket-name>/*"
          ]
        }
      ]
    }
    ```

    Make sure to replace `<bucket-name>` with the name of the bucket you created earlier.

1. Click *Validate Policy* to verify the syntax of the policy document. If everything is fine, click *Apply Policy*. You should see the new policy listed under *Inline Policies* for the group when you're done.

### Update settings

Save the AWS credentials that you wrote down earlier (*Access key ID* and *Secret access key*) to the configuration file you created at the beginning of this tutorial (`name-of-your-domain.yml`), and specify the bucket name:

```
LPD_AWS_ACCESS_KEY_ID: <access-key-id>
LPD_AWS_SECRET_ACCESS_KEY: <secret-access-key
LPD_AWS_STORAGE_BUCKET_NAME: <bucket-name>
```

## Generate secrets

1. Generate Tarsnap key for LPD logrotate backups. The key shouldn't end up on the LPD server, but instead store it somewhere safe (e.g. in secrets).

1. Generate Tarsnap read write keys from the master key, see [tarsnap-keymngmt](http://www.tarsnap.com/man-tarsnap-keymgmt.1.html).

1. Go to Dead Man's Snitch at https://deadmanssnitch.com/ and generate two snitches, save them under:

    - `LPD_TARSNAP_SNITCH` --- this will monitor saving logs, this snitch should have daily interval
    - `SANITY_CHECK_SNITCH` --- this will monitor sanity check, this snitch should have 15 minute interval

1. Generate various LPD secrets:

    - `LPD_LTI_CLIENT_KEY` --- a random string
    - `LPD_LTI_CLIENT_SECRET` --- a random string
    - `LPD_PASSWORD_GENERATOR_NONCE` -- a random string

    You can use the following script to generate these secrets:

    ```
    #!/usr/bin/env python
    import string
    from django.utils.crypto import get_random_string
    get_random_string(64, string.hexdigits)
    ```

## Perform deployment

From root directory of [`ansible-secrets`](https://github.com/open-craft/ansible-secrets/) repo, run:

```sh
# If necessary, initialize submodules
git submodule update --init -- deploy
git submodule update --init -- group_vars

# ... or simply update them
git submodule update -- deploy
git submodule update -- group_vars

# Create virtualenv (optional if you already have a venv for deployment)
virtualenv venv

# Activate venv
. venv/bin/activate

# Update dependencies
pip install -r deploy/requirements.txt

# Run playbook
ansible-playbook -vvv deploy/playbooks/deploy-all.yml --private-key <ec2-ssh_key> --limit <lpd_domain>
```

Shell into EC2 instance, make sure that all necessary settings are present in `app/local_settings.py`, and replace the default LDA model and tf-idf vectorizer (which are just empty Python pickle files) with the files you want to use for your LPD deployment (as described in LPD's [README](https://github.com/open-craft/learner-profile-dashboard)).

If you change any settings, run:
```
sudo systemctl restart gunicorn
```

## How to redeploy master to production / change branch (on stage)

1. Shell into EC2 instance using SSH key.

1. Become root:
    ```sh
    sudo su
    ```

1. Get to proper directory:
    ```sh
    cd /home/lpd/repository
    ```

1. Change branch via
    ```sh
    git checkout <feature-branch>
    ```
    or pull in latest changes via
    ```sh
    git pull origin master
    ```

1. (Optionally) Remove all `*.pyc` files:
    ```sh
    find . -name '*.pyc' -delete
    ```

1. Activate virtualenv:
    ```sh
    . ../venv/bin/activate
    ```

1. If needed, update requirements:
    ```sh
    pip install -r requirements/base.txt
    ```

1. If needed, run the migrations:
    ```sh
    ./manage.py migrate
    ```

1. If needed, collect static assets:
    ```sh
    ./manage.py collectstatic
    ```

1. Restart Gunicorn:
    ```sh
    systemctl restart gunicorn
    ```
