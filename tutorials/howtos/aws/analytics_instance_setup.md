# Background and prerequisites

Purpose: This document describes how to set up a complete analytics stack on a single VM, suitable for development/sandbox purposes.  For production deployments, see [AWS Analytics Setup](AWS_analytics_setup.md).

This document is based on [edX Analytics Installation][edx-analytics-installation] document. As base document assumes,
a working LMS-Studio or LMS-Studio-ecommerce instance already created.

[edx-analytics-installation]: https://openedx.atlassian.net/wiki/display/OpenOPS/edX+Analytics+Installation

# Prepare LMS

* Create user that can connect from the world and grant readonly access to DBs:

        admin$ mysql -u root
        CREATE USER 'insights_ro_user'@'%' IDENTIFIED BY 'insights_ro_user_password';
        GRANT SELECT on edxapp.* to 'insights_ro_user'@'%';
        GRANT SELECT on ecommerce.* to 'insights_ro_user'@'%';

* Preserve these access credentials for the analytics task configuration file `edxapp_creds`, as described in
  [`analytics_jenkins_jobs.md`](analytics_jenkins_jobs.md).
* Allow [EMR access to RDS](AWS_deployment_tutorial.md#emr-access-to-rds).

# Create an analytics sandbox instance

**TODO**: These manual steps need to be replaced with Terraform code. All new servers should be defined and managed by Terraform. See [Provider: OVH](https://www.terraform.io/docs/providers/ovh/index.html) and [Provider: OpenStack](https://www.terraform.io/docs/providers/openstack/index.html)

Do not manually provision any new servers using the steps below! Instead please do it using Terraform and then replace these instructions with the new Terraform-based instructions. If you don't have time/budget to fully use Terraform, then please at least partially use Terraform and make sure each new deployment is using more Terraform than the last. See [the Terraformed Infrastructure epic](https://tasks.opencraft.com/browse/SE-2436) for details.

Opencraft Instance Manager is not able to provision analytics instances so far, so you've got to do that ~~manually~~ using Terraform.

> * Log in to https://horizon.cloud.ovh.net/ using credentials provided in https://github.com/open-craft/secrets (by the
>   time of writing it contained two credential sets: use the one with "Dev")
> * (If not already done so) Create ssh access key - Access & Security -> Create Key
>   * Provide name
>   * Wait for OVH to generate SSH key and download it
>   * (Optional) Share with the team by uploading somewhere (secrets repo?)
> * Instances -> Launch Instance
>     * Details
>       * Name - whatever you want
>       * Flavor - keep it to minimum; `vps-ssd-2` seems to be just enough (the smallest instance is `vps-ssd-1`, but it
>           has 2Gb RAM, while stock analytics-devstack uses 4Gb RAM by default, so `vps-ssd-2` is chosen)
>       * Count - 1
>       * Boot Source - From Image
>       * Image - Ubuntu 12.04 (from "edX Analytics installation": "NOTE: there are known issues upgrading to 14.04 –
>       changed package names, etc. They are probably easily solvable, but we haven't done it yet". If it's not there
>       anymore you might try to choose newer version at your own peril)
>     * Access & Security:
>       * Key pair - choose SSH key pair
>       * Security Groups - make sure instance is a member of security group that allows inbound ssh and http traffic
>     * Other tabs - default values are just fine, but make sure instance is attached to at least one network
>
> Click "Launch", wait for VM to be ready.

Verify you can ssh to the new VM:

     ssh -A -i <location/of/ssh_key.pem> admin@<machine_ip>

**Note:** `-A` enables agent forwarding, and `man ssh` warns that it should be used with caution. In this particular
case we're using it to simplify copying `tracking.log` files into analytics box from LMS box - see below. Copying
tracking.log is not an operation to be done every time you log in to analytics box, so you might want to omit `-A` most
of the time.

# Setup system packages and virtualenv

    admin$ sudo apt-get update
    admin$ sudo apt-get install git python-pip python-dev libmysqlclient-dev

    admin$ sudo pip install virtualenv

    admin$ virtualenv venvs/ansible

If you get `locale.Error: unsupported locale setting` it means that your the user you are logged in have some fancy
locale, not yet available on the server. [This askubuntu answer][set-user-locale] proved to be helpful (not sure if the
first answer works, but the second works for sure)

[set-user-locale]: http://askubuntu.com/questions/162391/how-do-i-fix-my-locale-issue

# Run ansible setup script

Prepare ansible environment:

    admin$ . venvs/ansible/bin/activate
    admin$ git clone https://github.com/edx/configuration.git
    admin$ cd configuration/
    admin$ pip install -r requirements.txt

Set environment values:

    admin$ export LMS_HOSTNAME="[LMS_URL_WITH_HTTPS]"
    admin$ export INSIGHTS_HOSTNAME="http://127.0.0.1:8110"  # Change this to the externally visible domain and scheme for your Insights install, ideally HTTPS

Run the script:

    admin$ cd playbooks/edx-east/
    admin$ ansible-playbook -i localhost, -c local analytics_single.yml --extra-vars "INSIGHTS_LMS_BASE=$LMS_HOSTNAME INSIGHTS_BASE_URL=$INSIGHTS_HOSTNAME"
    # wait for it

## Troubleshooting

* Ansible: `failed: [localhost] => (item=/etc/update-motd.d/51-cloudguest) ...` - this file does not exists for non-AWS ubuntu images; just add it:
  `sudo touch /etc/update-motd.d/51-cloudguest` (it was [promised to be fixed][config-51-cloudguest], but not yet)

[config-51-cloudguest]: https://github.com/edx/configuration/issues/2242

# Verifying ansible setup

Check hadoop:

    admin$ sudo su - hadoop
    hadoop$ cd /edx/app/hadoop
    hadoop$ hadoop jar hadoop-2.3.0/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.3.0.jar pi 2 100

If you see something like `Estimated value of Pi is 3.12000000000000000000` - hadoop works just fine

Check Hive - as `hadoop` user:

    hadoop$ hive  # if, for some reason it's not on path, it should be at /edx/app/hadoop/hive/bin/hive
    hive> show tables;
    OK  # if you see this it works - there are no tables in Hive so far

(Optional) Check Insights:

    # Ctrl+D until you're `admin` again
    admin$ curl localhost:8110

Should dump Insights HTML markup in console. Also, you can access <VM_IP>:18110 from your local machine to check if
Insights is accessible from the rest of the world.

(Optional) Check data cron scripts works:

    # scp tracking.log onto the machine from the LMS.
    admin$ ssh user@[LMS_IP] "sudo cat /edx/var/log/tracking/tracking.log" > tracking.log
    admin$ sudo mkdir /edx/var/log/tracking
    admin$ sudo cp tracking.log /edx/var/log/tracking
    admin$ sudo chown hadoop /edx/var/log/tracking/tracking.log
    # wait a minute -- ansible creates a cron job to load files in that dir every minute

    # Check it
    admin$ sudo su hadoop
    hadoop$ hdfs dfs -ls /data

    Found 1 items
    -rw-r--r--   1 hadoop supergroup     308814 2015-10-15 14:31 /data/tracking.log

# Setup pipeline

Generate ssh key:

    admin$ ssh-keygen -t rsa -f ~/.ssh/id_rsa -P ''
    admin$ echo >> ~/.ssh/authorized_keys # Make sure there's a newline at the end
    admin$ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
    admin$ ssh localhost  # check it works

Bootstrap analytics pipeline

    # Make a new virtualenv -- otherwise will have conflicts
    admin$ virtualenv venvs/pipeline
    admin$ . venvs/pipeline/bin/activate

    (pipeline)admin$ git clone https://github.com/edx/edx-analytics-pipeline
    (pipeline)admin$ cd edx-analytics-pipeline

    (pipeline)admin$ make bootstrap

Check it works:

    (pipeline)admin$ remote-task --host localhost --repo https://github.com/edx/edx-analytics-pipeline --user $USER --override-config $HOME/edx-analytics-pipeline/config/devstack.cfg --wheel-url http://edx-wheelhouse.s3-website-us-east-1.amazonaws.com/Ubuntu/precise --remote-name analyticstack --wait TotalEventsDailyTask --interval $(date +'%Y') --output-root hdfs://localhost:9000/output/ --local-scheduler

# Configure pipeline to talk to LMS DB

Create/Edit `/edx/etc/edx-analytics-pipeline/input.json` with your favorite text editor so that it contains LMS MySQL
credentials:

    {"host": [MYSQL_HOST], "port": 3306, "username": "insights_ro_user", "password": "insights_ro_user_password"}

Alternatively, there's a script for that:

    admin$ cat <<EOF > input.json
    {"host": [MYSQL_HOST], "port": 3306, "username": "insights_ro_user", "password": "insights_ro_user_password"}
    EOF
    admin$ sudo mv input.json /edx/etc/edx-analytics-pipeline/input.json
    admin$ sudo chown hadoop:hadoop /edx/etc/edx-analytics-pipeline/input.json

Test it:

    (pipeline)admin$ remote-task --host localhost --user $USER --remote-name analyticstack --skip-setup --wait ImportEnrollmentsIntoMysql --interval $YEAR --local-scheduler

## Troubleshooting

* `ImportError: cannot import name python_2_unicode_compatible` when running analytics task - caused by outdated `six`
  version - fix with `/var/lib/analytics-tasks/analyticstack/venv/bin/pip install 'six>=1.10.0,<2.0.0'` as admin.

# Configure LMS to work with Insights

The idea here is to allow Insights from the analytics VM to use LMS as OAuth2 provider. Instructions are very much the
same as for Otto ecommerce OAuth2 integration:

* LMS:
  * Edit `lms.yml`:
    * `FEATURES[ENABLE_OAUTH2_PROVIDER]`: true
    * `OAUTH_ENFORCE_SECURE`: false  # or run LMS with HTTPS enabled
  * Log in to LMS admin as staff/superuser and go to `admin/oauth2/client/` -> 'Add Client'
  * URL: Insights HTTP(s) address, i.e. http://149.202.166.180:18110/
  * Redirect uri: Insights HTTP(s) address + `complete/edx-oidc/`, i.e. http://149.202.166.180:18110/complete/edx-oidc/
  * Client ID: copy/write it down and make sure it is globally unique.
  * Client Secret: copy/write it down
  * Client type: confidential (Web applications)
* Insights  - set client ID and Client Key
  * Edit `edx/etc/insights.yml` with your favorite text editor
  * Set `SOCIAL_AUTH_EDX_OIDC_KEY` to LMS' Client ID
  * Set `SOCIAL_AUTH_EDX_OIDC_SECRET` to LMS' Client Secret
  * Set `SOCIAL_AUTH_EDX_OIDC_ID_TOKEN_DECRYPTION_KEY` to LMS' Client Secret (and equal to
      `SOCIAL_AUTH_EDX_OIDC_SECRET`)
  * Restart Insights server: `sudo /edx/bin/supervisorctl restart insights`

Designate new client as trusted:

* Log in LMS admin as staff/superuser and go to `admin/oauth2_provider/trustedclient/` -> 'Add ...'
* Choose the client you have just added (they are identified by Client URL)

Open Insights, click "Login" in top-right corner. You should be taken to LMS login form. Complete LMS
login, and you should be redirected back to Insights, logged in as the same user.

## Troubleshoting

* 404 NOT FOUND when trying to log in to Insights - make sure `FEATURES[ENABLE_OAUTH2_PROVIDER]` is set to true.
* `access_denied A secure connection is required.` LMS error when trying to log in - switch LMS to HTTPS or allow
  insecure OAuth requests with `OAUTH_ENFORCE_SECURE`: false.
* `AuthTokenError: Token error: Signature verification failed` - double check that `SOCIAL_AUTH_EDX_OIDC_ID_TOKEN_DECRYPTION_KEY`
  is equal to `SOCIAL_AUTH_EDX_OIDC_SECRET`.
* Insights OAuth2 fails with `AuthTokenError: Token error: Invalid issuer` (in /edx/var/log/insights/edx.log) - make
  sure  Insights' `SOCIAL_AUTH_EDX_OIDC_URL_ROOT` matches perfectly with LMS' `OAUTH_OIDC_ISSUER` (keep an eye on
  http/https).

# Running analytics tasks on the box

    # SSH to analytics box
    ssh -i [YOUR_SSH_KEY] admin@[BOX_URL]
    # activate pipeline virtualenv
    admin$ cd edx-analytics-pipeline
    admin$ . venvs/pipeline/bin/activate
    admin$ export WHEEL_URL=http://edx-wheelhouse.s3-website-us-east-1.amazonaws.com/Ubuntu/precise
    admin$ export BRANCH=[YOUR_BRANCH]
    admin$ remote-task --host localhost --user $USER --remote-name analyticstack --repo https://github.com/open-craft/edx-analytics-pipeline.git --branch $BRANCH --override-config ${PWD}/config/devstack.cfg --wheel-url $WHEEL_URL --wait <TASK> --local-scheduler <TASK_PARAMETERS>

Example:

    admin$ remote-task --host localhost --user $USER --remote-name analyticstack --repo https://github.com/open-craft/edx-analytics-pipeline.git --branch reports-with-coupons --override-config ${PWD}/config/devstack.cfg --wheel-url $WHEEL_URL --wait OrderTableTask --local-scheduler --import-date 2016-03-22
