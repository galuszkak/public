"""
Type annotations used primarily for validating YAML.
"""

from typing import TypedDict, List, Union, Optional


class Member(TypedDict):
    """
    All cell members will be expected to have these fields. This model may get more complex depending on how
    """
    name: str
    email: str


class TitledMember(Member):
    """
    Member with a specifically displayed title. Used for the MetaCell.
    """
    title: str


class ToolKit(TypedDict):
    """
    Links to standard tools each cell should have. These can be empty strings if the tools do not exist yet.
    """
    chat: str
    sprint_board: str
    backlog: str
    epics_board: str


class RoleAssignmentBase(TypedDict):
    """
    Base class to support :class:RoleAssignment.

    Note: This class isn't meant to be used and is only around to make these
    attributes required.
    """
    primary: str
    backup: str


class RoleAssignment(RoleAssignmentBase, total=False):
    """
    A role assignment can have a primary and backup assignee, and an optional
    secondary backup.
    """
    backup2: Optional[str]


class Roles(TypedDict):
    """
    Standard roles. These can be empty strings if no one is assigned yet. Each entry should be an email,
    or a list of emails, which will be checked against the cell's team to verify that the cell member exists.
    """
    recruitment_manager: Union[str, RoleAssignment]
    sprint_manager: Union[str, RoleAssignment]
    sprint_planning_manager: Union[str, RoleAssignment]
    epic_manager: Union[str, RoleAssignment]
    sustainability_manager: Union[str, RoleAssignment]
    ospr_liaison: Union[str, RoleAssignment]
    official_forum_moderator: Union[str, RoleAssignment]
    devops_specialist: Union[str, RoleAssignment]


class MetaRoles(TypedDict):
    """
    Roles for the metacell.
    """
    official_forum_moderator: str
    community_liaison: str
    sprint_manager: str


class Cell(TypedDict):
    """
    Defines information about a cell.
    """
    name: str
    members: List[Member]
    tools: ToolKit
    roles: Roles
    rules: List[str]


class MetaCell(TypedDict):
    """
    Structure for the 'Meta-cell', or where non-team members go.
    """
    members: List[TitledMember]
    roles: MetaRoles


class OpenCraft(TypedDict):
    """
    Defines information about the entire organization.
    """
    cells: List[Cell]
    meta_cell: MetaCell
