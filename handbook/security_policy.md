# OpenCraft Security Policy

## 1. Introduction

As a professional services company, ensuring the security of our clients'
information and the privacy of their users' data is critically important. This
security policy states the best practices, expectations, and requirements that
we expect each team member to adhere to based on their role.

Our security practices have these goals:

* To ensure that confidential, private, secret, and personal data is not
  disclosed inappropriately
* To ensure the privacy of our clients and our clients' users
* To ensure that infrastructure we maintain is not accessed nor exploited by
  unauthorized parties, e.g. tampering with account information
* To ensure that infrastructure we maintain is not utilized to attack users or
  third parties, e.g. cross site scripting used to attack site visitors
* To ensure that infrastructure we maintain is resilient against attacks

## 2. Roles and Responsibilities

### Security Coordination

Our CTO, Braden MacDonald, is responsible for developing OpenCraft's security
policies, for ensuring that we hold ourselves to a high standard, and for
coordinating our response to major security issues.

### Team Member Responsibilities

Each team member is responsible for:

* Avoiding disclosure of confidential information
* Following the security policies defined in this document
* Reporting security vulnerabilities they discover (to OpenCraft or to the
  relevant upstream maintainer)

## 3. Definitions

### Information Classification

Information that we get from clients or users is classified as follows:

**Public**: Public information, open source code, etc. Anything that can be found
in a Google search and has been intentionally published by the owner/author.

**Confidential**: Any information which is not clearly public. This includes
information that is intended to eventually be public, but it not yet ready to be
published.

Confidential information can be shared with anyone on the OpenCraft team or with
the relevant client but must never be disclosed to other clients nor to the
public.

**Private**: Information that is clearly private and would never reasonably be
expected to be published publicly, such as user personal information (user
names, email addresses, online activity, etc.), system access credentials, or
proprietary source code.

Private information sometimes must not even be shared with certain employees of
the relevant client; if in any doubt about who can see private information, ask
the CTO or CEO via JIRA or email.

**Secret**: Highly private information granted on a need to know basis. Must
be marked as "Secret" and the list of people authorized to view it should be
specified on the document/information itself.

Never share secret information with anyone who is not explicitly authorized.

## 4. Policies

### Confidential Information

Any information about our clients, their business, their customers/users, their
future plans, etc. that is not clearly public should be treated as confidential.

### Laptop/Machine Security

Any OpenCraft-related data stored on a team member's laptop or other devices
must be encrypted (use full disk encryption or an encrypted partition/folder).

All of our team member's devices that contain OpenCraft-related data must have
some sort of auto-lock enabled (require password/biometric/etc. after certain
amount of time).

The operating system and other software used by team members on their own laptop
or other devices must be kept up to date with all available security patches,
and must follow best practices for securely configuring their choice of
operating system. If using Windows laptops/machines, they must have a firewall
and up to date anti-virus software.

### Passwords

Team members must use sufficiently random, unique passwords for services/systems
that they use for OpenCraft work, and must store the passwords in a safe
location, such as a vetted and trustworthy password manager.

### Two Factor Authentication

OpenCraft employees must configure and use two-factor authentication for any
cloud services used by OpenCraft that support two-factor authentication, such as
Google Apps and Amazon Web Services.

### HTTPS

All publicly accessible websites and web applications set up by OpenCraft must
allow HTTPS connections, the default protocol used should be HTTPS, and HTTP
connections should redirect to HTTPS.

### Security Clearance

Team members are only to get access to our KeePass database of client
credentials upon successful completion of their trial period. Until that time,
they should obtain credentials from other team members on a strictly as-needed
basis.

### Sending Sensitive Information to Clients

When passwords, API keys, or other short anonymous pieces of sensitive
information need to be send to clients, the data must either be encrypted or
sent partially by email and partially by https://onetimesecret.com/ . For
example, send the username by email and the password via onetimesecret; never
include a username and password pair in a onetimesecret.

When private or non-anonymous data (e.g. exports of student data or courseware)
needs to be sent to a client, it must be transmitted securely and not sent by
email. For example, it could be shared by Google Docs or encrypted and then
emailed.

### Changes to Security Policy and Deviations

Changes to our security policy must be approved by the CEO and CTO.

Deviations from the security policy (i.e. using a less secure approach
temporarily for a particular client/project/team member) must be approved on
JIRA by either the CEO or CTO. Deviations will only be approved in cases where
no other reasonable route is possible.

### Disaster Recovery

All of OpenCraft's infrastructure should be designed so that the loss of any
major provider (or provider's data center in the case of AWS) should not cause
any permanent data loss.

For example, we currently use OVH as an OpenStack provider but we use only
standard OpenStack APIs and store complete backups on S3 using Tarsnap, so that
if OVH were to be offline for a long period of time or to go out of business,
we could move all our instances and the instance manager to any other comparable
OpenStack provider.

### Offboarding

Following the departure of a team member, OpenCraft staff must complete the
defined [offboarding procedure](https://gitlab.com/opencraft/documentation/private/blob/master/onboarding_steps_manager.md#offboarding-checklist)
in full within ten days of the team member's last day. Any passwords that that
team member had access to must be changed.

### Security Audits

At least every year, OpenCraft shall conduct an internal security audit. Each
audit should verify each policy listed in section 4 of this document, as well as
investigating the security of OpenCraft's infrastructure in other ways. The
audit should result in a written report and any deficiencies identified should
be resolved as soon as possible.

### Annual security policy review

Every year, the CTO and 1-2 other team members shall review the security policy
and propose revisions, which will then be considered for approval by the CTO and
CEO.

## 5. Procedures

### Reporting a Security Vulnerability

If you are aware of a security vulnerability affecting our clients or
infrastructure, please:

* Open a ticket on JIRA with priority CAT-1. Put "SECURITY" in the ticket name.
  Put in as many details as you have, and don't worry about filling in all
  fields correctly.
* Assign the ticket to the designated sprint firefighter, if possible
* Immediately notify Braden MacDonald (CTO) by email and copy ops@opencraft.com.

### Reporting a Security Breach

If you are aware of a security breach (someone has accessed client/OpenCraft
data/systems that they should not be able to access), immediately notify
security at opencraft dot com.

### Response to a Security Breach

The current DevOps on-call team will be responsible for responding to any
security breach. The response must mitigate the breach, inform any affected
client(s), preserve as much event data as possible for forensic analysis, open
an incident report ticket on JIRA, and rotate any compromised
credentials/systems. Following any such incident, the CTO will work with the
team members involved to complete a root cause analysis and action plan to
prevent similar incidents in the future.

### Risk Identification and Mitigation

All team members are encouraged to:

* Follow security mailing lists & announcements from edX Inc., Django, AWS, OVH,
  and other relevant organizations.
* Notify the CTO by email of any potential risk to OpenCraft/client/user data or
  systems, even if that risk is not a formal security vulnerability.
* Document such risks in relevant pull requests and/or JIRA tickets.
* Get approval from the CTO or CEO before merging code / deploying systems with
  an increased risk of data/system exposure.
  * The intention of this is that contracts/insurance can be adjusted as
    appropriate.
