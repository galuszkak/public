# Client: *Example University*

## Points of Contact:
* **Primary**: Person McPersonface someone@example.com
* **Billing**: Penny Wise billing@example.com
* **Client Owner**: Devy Developerson

## URLs:

* **CRM Entry:** *Link to CRM entry*
* **LMS:** https://lms.example.com/
* **Studio:** https://studio.lms.example.com/
* **Website:** https://example.com/

## Projects/Current Epics:

* Hosted OCIM instance (see LMS link)
* Missingno XBlock https://github.com/example/MissingNoXBlock
* *Link to Wormhole creation epic*

## Background

Example university is a theoretical university that has contracted our services. They've been in business for -100 years and have decided to move to online learning after a plague broke out and their filing cabinets full of transcript records rusted through the floor.

They have been our client for 3.14159 years and have a history of wanting custom XBlocks and features that break fundamental assumptions of spacetime.

They pay us before we issue them invoices, and we deliver work yesterday. Please do not mention medical professionals when speaking to them. Person McPersonface freaked out when we mentioned a 'Doctor' at one point and it was very difficult to calm him down.