# Onboarding Email Template

Hi [NAME],

As promised, we have given you access to the team's tools. You should have received an email from Google on your personal email, giving you access to your email @opencraft.com, where you should receive an email from each of the other tools to set up your account.

In terms of timeline & next steps:

1. I will post on the forum shortly to announce your arrival to the whole team.
1. To follow-up on your onboarding, could you schedule one-to-one meetings ("121s") with Xavier? You can do this directly in his calendar (cf https://calendly.com/antoviaque/30min ), at a time that works for you:
     1. A meeting 1-2 weeks after your start date - this will be to check on you, see if there is anything you need or that could help you.
     1. A meeting 2 months after your start date + 2-3 days - this will be the end of an informal "trial period", where we would review how things have gone for you, and the rest of the team.
     1. At any time, don't hesitate to ask me, Xavier or Braden (the CTO), or anyone in the team if you have any questions. Also, feel free to book a 121 at any time in our calendars if there is anything you would like to discuss! You will also soon be paired with a dedicated mentor, with whom you will meet regularly.
1. Can you also make sure that your computer is going to be able to match the tech requirements of the Open edX stack and allow for proper collaboration? You will need at least this configuration:
     * Multi-core CPU with hardware virtualization support (VT-x/AMD-V)
     * 16+ GB of RAM to run Docker containers/VM
     * 50+ GB free disk space on an SSD drive, with an encrypted home partition/drive
     * Reliable internet connection with enough bandwidth for video calls (at least 4.0 Mbps download and 3.2 Mbps upload, per https://support.google.com/a/answer/1279090?hl=en)
   * Good dedicated external microphone (either part of a headset or standalone microphone)
1. [IF NOT IN APPLICATION:] Can you send me your GitLab account name, creating one if you don't have one already? You will need it to access the repositories we store there.

I have also created your onboarding task: [https://LINK.TO.ONBOARDING.TASK/] (you will need the access you should have received in your @opencraft.com mailbox).

If you have time to start looking at this ahead of your start date, especially the devstack setup which is often painful at first, it would be very useful. It shouldn't be more than a day or two of work in total, but it's a lot of reading and set up, so you might actually appreciate being able to do it progressively, rather than packed in an intense first day.

Good luck with absorbing all of this -- and welcome again to the team : )
