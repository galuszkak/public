# Time Management

OpenCraft's sprints are jam-packed with client tasks, forum discussions, mentoring, context switching, planning for next
sprint, epic project budgets, firefighting, discoveries, more context switching.. in short, there is a lot of work to
do, and only one sprint to do it in. So how do we do it all, and not go crazy?

The short answer is, we all go crazy some of the time :) But the goal is to find your own balance, with sustainable work
practices that improve your productivity and lower your stress.

On this page, we'll share some tips and strategies for time management.

## Take real breaks

Really. Sometimes the best way to manage your time, especially when everything feels overwhelming, is to walk away from
your desk for a while -- mentally, as well as physically.

If you're having trouble working through a sticky problem, or finding it difficult to focus, or getting stuck down in
the weeds of a ballpark estimate instead of staying up at the high level, that's a good time to take a break. Do
something that really does take your mind away from the pressing issue: take a walk, listen to a podcast, read something
unrelated, have a cup of tea, pet your cat, talk to a friend.

As with everything, the more breaks you take, the better you'll get at taking them, and at achieving the mental block
release that they provide.

## Assess your sprint

Every couple of days at least, it's a good idea to take stock of how your sprint is going.  We suggest you print out a
copy of [Are you feeling productive?] and keep it near your desk, as a reminder.

1. Are you feeling unproductive?
1. Are you running behind at all?
1. Is this task taking longer than you estimated?
1. Is this task more complex than it sounded?
1. Are you doing more than the task requires?

If the answer to one or more of these questions is "yes", then it's time to do something about it.

1. Who are the **two** people you should tell?<br>
    Some suggestions are:
    * Task reviewer
    * Epic owner/reviewer
    * Sprint firefighter
    * Upstream specialist
    * Braden
    * Xavier
1. Can you split added scope into another task?<br>
    If yes, propose the split to:
    * Task reviewer
    * Epic owner/reviewer
1. Can someone pick up any of your other tasks? To find out, ask:
    * Sprint firefighter
    * On Mattermost
    * During the mid-sprint meeting.

## Plan your sprint

If you're consistently having difficulty finding time to complete tasks, or are always working in the evenings or on
weekends when you don't want to be, then you might need to work on your sprint planning.

Take care to note the timezone and schedule of your reviewer, so you can limit round-trip time for replies. Be sure to
discuss when you'll have the work ready for review, especially if your reviewer will be away for part of the sprint.

One approach is to map out the days in your sprint, and as you assign yourself to tasks and reviews in the upcoming
sprint, block out the day(s) each task will cover. To limit context switching, try not to have more than 2-3 different
tasks in a day, especially at the beginning of the sprint.

You may find another approach works better for you, but the important thing is to have a strategy, and tune it to
ensure that it helps your sprint and stress levels.

## Improve task estimates

If you're using the [Sprints app] to avoid overcommitting, but you find that you're consistently logging more
hours than you initially estimated, then you might need to work on improving your task estimates.

One of the common traps when taking on a task is to "estimate" it by looking at the hours you have remaining on the
[Sprints app], and using that to determine how much time the task will take. This consistently skews
estimates into being too small, so don't do it! Please evaluate each task independently, based on time required to
achieve its acceptance criteria, not how much time you have left to do it.

If the scope of your tasks grows beyond what you expected the task to be, then it's best practice to split this extra
work into another task. Splitting tasks isn't just about semantics, this is part of how we keep our client commitments
within bounds and on time.

Use JIRA to list [your tasks which had more hours logged than originally estimated][jira own tasks above estimate], and
at the end of each sprint, spend some time assessing them to see if you could have done anything differently. Ask your
reviewer or the epic owner if they have any ideas too.

[How to do estimates](how_to_do_estimates.md) includes a list of common items that require logging time. With each task estimate, there are the set factors which are known in advance and unlikely to change and there are the risk factors which are unknown.

## Improve task estimates, a checklist

Use the point estimation tables at [Task workflows](task_workflows.md) to get fast ballpark numbers for most tickets.

### Use this checklist

* **when** you have exceeded the estimate on a ticket, or,
* **when** you are chronically underestimating for a particular client or project.

### How to use this checklist

1. Go down the known column. Enter the time you expect to spend on each item.
1. Sum that column. This is your ticket estimate. If it is:

    * beyond the client budget, go to the client or to the epic owner and say so. Propose a simpler alternative if one exists. Remember: this is the **actual** time you expect to spend, so if you hide it, you are preventing yourself from being paid a fair wage for your labor.
    * beyond the amount of time left in your sprint, timebox it or split it into smaller tasks. Discuss with the client if deadlines will be impacted.

1. Go down the risk column. Enter the extra time you will spend fixing things. The risk column is the upper bound of how badly a task can go wrong.
1. Sum that column. Divide by the known column, for example: **42 hours risk / 6 hours known = risk factor of 7**.
1. Brainstorm strategies for coping with risk before you start work.

    * Some work can be billed internally, like Ocim breaking.
    * Plan a rollback so that you don't leave the client in worse shape if you need to back out.
    * Politely schedule new work in new tickets. Don't permit scope creep.
    * Start risky work when you feel fresh. Not on Friday nights.
    * Get the client involved early. "We promised X but X also needs Y. Y takes ___ hours to build that we didn't estimate. Do you want Z instead?"

No matter how tight a client's budget is, they will appreciate a discussion of risk. This honesty builds trust. The opposite: hiding the risk and hoping for the best, will destroy trust when budgets and deadlines are exceeded.

Tip: As you go through the checklist, visually picture yourself doing each task.

| Task and Considerations | Known | Risk |
|-------------------------|-------|------|
| Setup and onboarding<br><br>\* A working devstack?<br>\* Read the code?<br>\* Read the existing JIRA comments or other docs?<br>\* All the permissions you need?<br>\* Risk factor: What will you do if your devstack won't provision?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2"> hrs |
| Implementation<br><br>\* New code for new features?<br>\* Refactor existing code?<br>\* Find and delete old code?<br>\* Writing tests: take your first estimate and double it :)<br>\* Risk factor: What will you do about unexpected code complexity?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Coordinating with the client<br><br>\* Minutes per email?<br>\* Number of emails?<br>\* Risk factor: What do you do about long discussions of unclear requirements?<br>\* Risk factor: What do you do if the client wants a video call?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Manual testing<br><br>\* Go through the full checklist?<br>\* Provision an appserver<br>\* Risk factor: What will you do if Ocim is not working?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Creating documentation<br><br>\* Code comments?<br>\* Ticket comments?<br>\* Extending READMEs?<br>\* Research?<br>\* Formatting?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Creating the pull request and sandbox<br><br>\* Good testing instructions?<br>\* Rebase?<br>\* Risk factor: What will you do if the CI is broken?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Code review<br><br>\* Reviewer onboarding?<br>\* Time for manual testing?<br>\* Time for reviewer to read code?<br>\* Risk factor: What can you do if the reviewer asks for significant changes?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Addressing feedback from the code review<br><br>\* Update testing instructions?<br>\* Provision new appserver?<br>\* Rebase?<br>\* Write comments on ticket?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Deploying<br><br>\* Run or update deployment playbooks?<br>\* Risk factor: What do you do if the deployment fails:<br> \*\* *related* to your change?<br>\*\* *unrelated* to your change?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Upstream contributions<br><br>\* How many review cycles?<br>\* Over how many sprints?<br>\* How many hours per each?<br>\* Risk factor: What will you do if upstream requests a different approach?| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |
| Extra work<br><br>\* Emails, Mattermost, sprint refinement| <input type="text" size="2">&nbsp;hrs | <input type="text" size="2">&nbsp;hrs |

## Spillovers

"Spillovers" are tasks which didn't reach "External Review", "Deployed & Delivered", or "Done"  by end of sprint. There's
several reasons why this might happen, and everyone on the team will have spillovers from time to time. But the goal is
always to reduce and eliminate spillovers, and have clean sprints.

### Why are clean sprints so important?

At OpenCraft, we deliver what and when we promise. All our projects and tasks are planned based on expectations of work
being completed in a certain timeframe. Not just our clients, but also our internal sprint managers, sustainability
planners, and epic owners and reviewers need to know that we can complete our commitments on time.

That's why, at the end of each sprint, we spend time going through the list of tasks that weren't completed during the
sprint, and asking ourselves, "What could we have done differently to prevent this task from spilling over?" The use of
the word "we" is intentional here, because a sprint is a team effort, and so spillover is not a single individual's
failing. Task reviewers, mentors, and sprint firefighters all have time set aside to help complete tasks, and it's ok
to lean on them and ask for help.

This is also why we have a [minimum spillover allowance for newcomers](https://handbook.opencraft.com/en/latest/processes/#evaluation-criteria), because we need
everyone to take spillover seriously, and to know that everyone can improve.

### How to address spillovers?

Here are some of the ways to reduce and eliminate spillovers.

1. Track your own spillovers and reasons.<br />
   This makes it possible to identify the most common reasons for spillover, and to figure out what you need to work on.
1. Improve your task estimates.<br />
   If you're consistently logging time over your tasks' Initial Estimate, then this makes the [Sprints app]
   a much less useful tool. See [Improve task estimates](#improve-task-estimates) for ideas and tips.
1. Take on tasks which are related to reduce context switching.<br />
   If there are several related tasks in the upcoming sprint, try to take on as many as possible to reduce context
   switching and setup time. If someone has already taken one or more of these tasks, then feel free to discuss and
   swap assignees around. It's always ok to ask.
1. Ask for advice.<br />
   If you're not sure what you could have done differently to avoid spillover on a task, ask your reviewer for
   recommendations and feedback, both early in the sprint and when a spillover is
   apparent. We should also always pay attention during the sprint review meetings for ideas from other people and their experiences, so that
   similar mistakes can be avoided.
1. Avoid scope creep.<br />
   This is easier than it sounds, because often, the scope creep comes from us being good engineers, trying to do
   more than the task requires. Sometimes scope creep comes from consultation with the client, but it's still ok to
   manage expectations and negotiate these additions if they come up.
   When you [assess your sprint](#assess-your-sprint), watch out for opportunities to split follow-up work into a new
   task, to be completed next sprint, and ensure that these tasks are communicated to the epic owner and/or client.
1. Limit review cycles.<br />
   Discuss the task with your reviewer early in the sprint if there's any ambiguity about the acceptance criteria.
   [Plan your sprint](#plan-your-sprint) to accommodate timezone disparities or any planned time off.
1. Use timeboxes to get started on tasks that you don't have time to complete.<br />
   Ideally, a task would be split in two so that something is completed during the sprint, e.g. if a task can be split
   into a small timeboxed discovery or prototype task, and an implementation task. This *must* be done before the start
   of the sprint and in consultation with the epic owner, but it's a good way to maintain momentum on a project.

[Are you feeling productive?]: https://docs.google.com/document/d/1aIle4F5itEcmPY0xKg3zXB0Yo8ySQ6KLosPmFCm_2cY/edit?usp=sharing
[jira own tasks above estimate]: https://tasks.opencraft.com/issues/?filter=-1&jql=assignee%20%3D%20currentUser()%20AND%20issueFunction%20in%20expression(%22%22%2C%22timeoriginalestimate%20%3C%20timespent%22)%20order%20by%20updated%20DESC

[Sprints app]: https://sprints.opencraft.com

## Tips and tricks

Time management is an essential skill which enables an individual to complete more in a shorter period of time, lowers stress and leads to career success. When working under the agile methodology, like we do at OpenCraft, it also helps in avoiding spillovers.

Some of the online courses that cover topics relevant to time management are:

* [Get Beyond Work-Life Balance (Inclusive Leadership Training)](https://www.edx.org/course/get-beyond-work-life-balance-inclusive-catalystx-il4x-1)
* [Work Smarter, Not Harder: Time Management for Personal & Professional Productivity](https://www.coursera.org/learn/work-smarter-not-harder)
* [Mindshift: Break Through Obstacles to Learning and Discover Your Hidden Potential](https://www.coursera.org/learn/mindshift)

Since there is no one-size-fits-all kind of solution for all these problems, we can share the best practices that the team members follow and try to pick and practise whatever is relevant to one's situation. We have forum threads about [working remotely](https://forum.opencraft.com/t/working-remotely-tips-and-tricks/112), [time logging practices](https://forum.opencraft.com/t/time-logging-practices/295/6) and [reducing stress](https://forum.opencraft.com/t/reducing-stress/272/22), all of which contain useful tips for managing time. We also have some content in the onboarding course about this in the [words of wisdom section](https://gitlab.com/opencraft/documentation/courses/blob/onboarding/html/opencraft_words_of_wisdom_text.html) and the [section on time management and invoices](https://gitlab.com/opencraft/documentation/courses/blob/onboarding/sequential/time_management_and_invoices.xml).

Below is a list of some of the best practices recommended by the team members:

* Capture all the time spent on work and work-related things and log all of it under the appropriate tickets. If there are restrictions due to the budget, timeboxes etc., check (and ask someone) if it is okay to spend a non-trivial amount of time on it before doing it. Under-reporting the worked time only hides the problems and causes people to lose quietly.

* Working from 9 to 5 doesn't necessarily equate to 8 hours of logged work. In practice, the actual amount of time spent to complete 8 hours of logged work is usually more.  This is because of factors like breaks, distractions  etc. taking some time which cannot be logged. So it is good to be focused and mindful about the time directly spent working on tickets. Having some form of a routine for work is usually helpful and it is important to come up with a routine that works for you.

* There is a non-stop flow of emails in one's inbox and dealing with reading all of them - at least the ones relevant to us, can be disruptive and challenging. Since we have 24 hours of time to respond to emails, it is not necessary to keep checking emails non-stop and trying to read all of them asap. Unless you are the current firefighter, it is also not necessary to read all the emails arriving on the `help@opencraft.com` mailing list. It is okay to occasionally scan for unanswered threads or subjects which might involve you. Having a few fixed slots of time every day to check and catch up on emails will reduce the distractions and context switches. Try out Gmail's Snooze feature, too -- you can clear your inbox and get reminded about things on the actual day you need to follow up.

* Serializing the asynchronous workload can make the work schedule more predictable and manageable.

* For work spread over many tickets, like during sprint planning, or reading ticket updates/emails, the best is to do this in one chunk, time that, and roughly divide the time between the main epics/topics that took the most time. For meta-work less than say 15 minutes, it is easier to log that time on the next ticket one picks up for working on.

* Planning ahead and working in advance: Prepare for future high workloads during times of low workload by working ahead. This will limit the percentage of time one will feel behind, and thus stressed.

### Scenarios and solutions

#### Handling new tasks that appear during a sprint

When one or more new tasks are injected during a sprint, discuss with the sprint manager and epic owners to pull tasks out of the sprint to accommodate the new ones. It might also be possible to hand over some assigned tasks to the firefighters to free up time for the new work.

#### Planning a sprint (and also after spillovers)

When planning a sprint after having spillovers in the previous sprint, work from the right side of the sprint board - i.e., tasks that are closer to being completed should be worked on before the tasks that are still in the 'backlog' state, the leftmost column.

Discuss the schedule for completing each task in the sprint with the respective reviewers. That allows everyone to plan and prioritize their work accordingly. While the ideal 'all tasks should be ready for review by the Wednesday of the 2nd week' scenario may not be possible to achieve in every sprint, planning the schedule in advance minimizes the risk of unexpected delays.

Since we are a globally distributed team, a schedule also helps navigate the delays in getting responses from team members who are in different time zones.

Go over all the tickets in your sprint, assess the work to be done, clarify the completion criteria, identify the potential risks and delays and discuss these with the reviewer to come up with a way to deal with them.

#### Keeping up productivity during the sprint

The ["Are you feeling productive?" chart](https://docs.google.com/document/d/1aIle4F5itEcmPY0xKg3zXB0Yo8ySQ6KLosPmFCm_2cY/edit) that Jill uses is a good tool for determining the next steps in case there is a loss of productivity or any unexpected issues.

It might also help to do routine productivity checks at fixed times to objectively check how many loggable hours one has worked on and if it is lesser than an expected number, change context and do something else, not necessarily related to work. For example, if one has been working since 9 AM in the morning and at 12 PM the amount of loggable work is less than expected, identifying why that is the case and then changing context after that could improve productivity.

#### Handling feature creep and scope changes

This basically falls into two categories: the client decides they want something extra mid-sprint or you realize mid-task that unplanned work is necessary to complete the task.

In either case, treat this as new work coming into the sprint. Either it or something else must go.

A good strategy for communicating with the client is:

* express empathy: "I know (fixing this logo, debugging these urls, . . .) is (urgent, important, what we agreed on)."
* explain the problem, quickly and without jargon: "This (forum version, theme, LTI, etc) is incompatible with your (library-name) which does (thing). I will need to upgrade (library-name)."
* explain what has changed: "I estimate this will take an extra X hours, which is (beyond the budget, more than I can fit in the current sprint)."
* make sure that they know they are in control: "Should we (cancel the upgrade, resume work next Monday, go with the default theme) or (other sensible option if one exists)?"

When you have repeated problems with this, the [estimation checklist](/time_management/#improve-task-estimates-a-checklist) in the handbook can help.

#### Planning for work disruptions like forum posts and discussions

If one ends up spending too much time on unplanned work like responding to forum posts and discussions, it might be a good idea to timebox such work to an hour or two for every sprint to minimize any side-effects. Similar to the suggestion for work spread over multiple tickets, it could be effective to do this in a timed chunk and roughly divide the time between the main topics that took the most time.

#### Taking holidays

It is important to take holidays periodically to get away from work, refresh and relax. There is more to life than just work and hence it is important to take a sufficient number of holidays every year. The process for requesting time off is documented [here](http://handbook.opencraft.com/en/latest/vacation/).

#### Work-life balance

Since it is possible to work at any convenient time in a remote job like ours, it is important to identify the importance of having a good work-life balance. Failing to pay attention to this can cause issues like stress, burnout, under-performance and impact health and well-being. Some of the suggested ways to help with maintaining a good work-life balance are:

* Setting aside a very specific work routine and timings and sticking to it.
* Working only on the weekdays and not working during weekends and holidays no matter what.
* Getting at least a good 8 hours sleep every night and on time. The key is to prevent long-term elevation or abnormal regulation of stress-related biological factors and to maximize the time spent in the resting state. From what we know so far, it’s pretty much grandma’s advice: Sleep well, eat and exercise in moderation, and engage in activities that help you feel relaxed and rested.

It is important to find what works for you and then to do it consistently. Consistency may be more important than intensity, so a brisk walk on a frequent and regular basis for reasonable amounts of time may be more helpful than jogging as fast as you can once every few weeks. Genuine support from friends and family can also be an effective chronic stress buffer.

---
<!--################################### Best Practices ###################################-->
## Time logging best practices

Logging time is required of every team member and can be
challenging. The granularity of the tasks is surprisingly fine to the
newcomer and even seasoned core members can be helped by the best
practices listed here. The spirit is that **all work should be logged**:

[Quoting Braden MacDonald](https://forum.opencraft.com/t/time-logging-practices/295/7):

> [...] ask yourself first is “should I spend X amount of time on
> this thing”; once you believe the answer is yes, then there’s no
> question that you should log the time because we don’t want people
> doing work they’re not getting paid for.

[Quoting Xavier Antoviaque](https://forum.opencraft.com/t/time-logging-practices/295/2):

> [...] there should not be any time that isn’t loggable. If you have
> to rework an upstream PR, that should go on the ticket from that PR,
> even answering emails or answering in the forums should go on a
> ticket (if there is no ticket for it, ask!). It might sometimes
> creates issue with budgets, but if you don’t log the time at all
> that will just hide the problem, and you will end up being the one
> who loses quietly - while if there is a budget issue, we’ll look at
> it together.

### Tips that help with time logging

These tips are related to recurring tasks or situations that require
time tracking and suggest how they can be approached. It is not a
policy and it is up to each team member to decide to apply them or
not.

#### Handling help@

For most people on the team, you don’t have to read the vast majority
of the messages on help@. It’s fine to ignore it almost completely,
and to only occasionally scan for unanswered threads or subjects which
might involve you. You could configure mail filters so help@ messages
never even arrive in your inbox unless you're explicitly CC’d or you
are watching the thread.

#### Checking normal emails with filters

You can have filters which automatically label each message with the
relevant client, and then it’s pretty easy to read all the emails from
one client at a time (e.g. I read all the green “Yonkers” emails in
various threads for 10min then log 10min on the “Yonkers Support”
epic; then I read all the blue Sophrogal emails for 15 min and log
that 15min on whatever Sophrogal epic the majority of those emails
were about; and so on…)

#### Checking incidents as they happen

Check the [Ops Review Draft](https://docs.google.com/document/d/1HLPwuRDcUHOUWcf1d7A2LAIs_s_BXNwkvo33MSdas88/edit)
for the current sprint and/or Mattermost to see if a ticket has already
been created for the incident in question.

If there is no ticket, either create it yourself using the appropriate
epic and account, or reach out to client and/or epic owners of affected resources
as necessary. They will be able to help you set the right epic and account
on the ticket (or simply point to you an existing ticket, e.g. for recurring
maintenance of a specific client instance, that you can use to log time).

#### Deciding whether to start a new discussion about XYZ

Deciding whether to start a new discussion about infrastructure,
reflecting about the work, learning about our infrastructure, reading
other tickets, technical problems during work which aren’t actually
related to any task etc. With all of these things, if it’s, say, much
more than 15 minutes and isn’t really related to anything you're
working on, you could just create a new ticket for yourself about
whatever it is and log the time on that.

### What should I log and how?

#### Logging more things

It is easy to overlook the time spent preparing the next sprint,
checking the timesheet, requesting for help in chat, reading e-mails,
posting in the forum etc. However, it’s still real work.

If you go a little over a timeboxed ticket, you should log it
instead of hiding it. You'll learn to stop earlier next time so that
you don't go over the timebox, that's better than getting used to
going over the timebox every time.

The exception is invoicing: things related to your own company's
functioning isn't something OpenCraft normally pays for (like the time
you spend doing your own company's accounting or taxes for example).

#### Look for recurring tickets

Some tickets are meant to be used to log recurring tasks such as the time
spent in sprint plannings meetings. When in doubt and before asking
for a specific ticket, go to the `Recurring` column of the sprint
dashboard and look for a ticket that may fit what you are doing.

#### Start to track your time immediately, before knowing where to assign it

When you sit down to work for OpenCraft, start to log immediately. The
time spent thinking about what you're actually going to work on,
preparing your environment by spawning a devstack, etc. is part of the work
(sometime called metawork). It also involves going through mail,
chat and forums to check in case something relevant came up. Whatever
ticket you start working first is where you should log this time.

#### Start to track your time right after completing a task

Unless you're done for the day, you start thinking about the next task
after finishing another. This context switching (or metawork) time
also involves participating in various communication channels and will
be included in the time logged for the next task.

#### Split work spread over many tickets after doing it

[Quoting Xavier Antoviaque](https://forum.opencraft.com/t/time-logging-practices/295/5):

> For work spread over many tickets, like during sprint planning, or
> reading tickets updates/emails, the best is to do this in one chunk,
> time that, and roughly divide the time between the main epics/topics
> that took the most time.

Another use case is if your preference is to get to zero inbox (forum,
chat etc.) once a day, it is likely that you batch the time spent
reaching that goal once a day and that it relates to a number of
tasks.

When you're done with this batch activity (processing mails, sprint
preparation etc.), take a step back and evenly divide the time spent
among all related tickets. For instance:

> You spend 90 minutes going through all mail, forum and chat. When
> you're done, you open the list of issues assigned to you or for which
> you are a reviewer. You pick 6 issues that you came across during
> these 90 minutes and log 15 minutes in each of them.

#### Do not log breaks that are real breaks

If you're working on a ticket for an extended period, it is healthy to
get up and move about a bit; something like 50 minutes working and
then 10 minutes break for instance. If you keep thinking about your
work, you should log this time. This applies even if you take those 10
minutes to do something unrelated, like [hang out some
laundry](https://forum.opencraft.com/t/reducing-stress/272/15) - when
the mental fog descends, those 10 minutes to step back are worth more
than an hour of going in circles and not progressing.

For breaks longer than 15 minutes spent doing something entirely
unrelated to work, like discussing with a friend, you should not log
that time because you're not working.

If you were at an office, this time would be paid regardless. This is
one of the reasons why a 9-5 working day at OpenCraft does not amount
to 8 hours of work being logged.

#### Don't worry about logging less than 15 minutes in the wrong ticket

When metawork (getting ready to work in the morning, context
switching, reading mail, fighting against a technical problem
preventing you from working, etc.) amounts to less than 15 minutes, it
is OK to log this time to any ticket you end up working on eventually.

[Quoting Braden MacDonald](https://forum.opencraft.com/t/time-logging-practices/295/7):

> [...] say I have just finished two hours of coding and I’m about to
> move on to my next task. I stop the timer in Toggl/Tempo and log the
> time I just spent, then start a new timer. Then I’ll [read a couple
> emails / forum posts / reply on Mattermost] and then start my next
> task. That may add 1-3 minutes to the new task, but that’s not a big
> deal, and it all averages out. However, if one of those emails
> balloons into something bigger and I end up spending twenty minutes
> on it instead of one minute, then I’ll just log that time to
> whatever ticket(s) is/are most relevant to that email(s), and start
> over.

[Quoting Xavier Antoviaque](https://forum.opencraft.com/t/time-logging-practices/295/5):

> [...] it’s not completely fair for the ticket you are taking up
> since half of that time is technically from the previous ticket, but
> everything doesn’t need to be precise to the minute, it will average
> out over time.

#### When a task longer than 15 minutes don't fit anywhere, create a ticket

It is not uncommon for a discussion to develop and require time to
develop further. When you sense it is likely to be the case, create a
ticket to log the time spent reading, thinking and writing on that
topic. This `Time logging best practices` page, for instance, is the
outcome of a discussion that was associated with a ticket where team
members logged their time on that topic.

[Quoting Braden MacDonald](https://forum.opencraft.com/t/time-logging-practices/295/7):

> “deciding whether to start a new discussion about infrastructure,
> reflecting about the work, learning about our infrastructure,
> reading other tickets, technical problems during work which aren’t
> actually related to any task”: with all of these things [...] if
> it’s [...] much more than 15 minutes [...], I would just create a
> new ticket for myself about whatever it was and log the time on
> that.

#### Avoid/reduce some of the activities which are hard to log

Helping with tasks when you are neither assigned to nor a reviewer when
not asked for is generous but it consumes part of the budget that
other participants will probably need later on.

### Testimonies

Randomly organized testimonies related to time logging which are
either inspiring or funny.

#### Brandon

I partially thought [worklogs] were in place to specifically be used
against me. It took me some time to fully come to terms with the
hourly stuff. Most of the guilt/frustration went away the more I:

* Trusted this stuff wasn't being used against me.

* Saw that other people on the team struggling with tech stuff and
  spillover like I was - I think it's important to figure out what
  "normal" really is on a team. It's not usually the superhuman thing
  one thinks it is initially.

At the end of the day, look through your activity history and sent
emails and make sure you didn't miss logging anything. When you're
bouncing back and forth between things rapidly, or there's a lot of
different issues grabbing your attention, it's really easy to miss
logging your time. I find that towards the end of particularly hectic
days, rather than feeling accomplished, I'm left feeling unsettled
about my time logs. It can be worth taking a quick look through your
activity history (even if you probably logged everything), just to
kick that feeling so you can start your work the next day more
refreshed.

#### Jill

I docked my hours when I felt like my struggles were the fault of my
own incompetence, and resented the job for "making" me do it. I viewed
the OpenCraft team members as superhuman — the amount of work you guys
get done, and how quickly and well you do it? Freaking amazing. The
level of scrutiny performed on even the smallest ticket, and the
quality of the code we produce, is incredible. I've never even had a
job where my code was reviewed, let alone criticised line by line, and
definitely not on public, open-source websites. Initially, when I
started this job, I'd have to take a deep breath before replying with
a "thank you for spotting that!", but now, I love seeing how clean the
resulting code is once it's been through our wringer. I'm proud of
what I create, and prouder now when it survives the reviews. But that
took time.
